"""This code create a world class for ant foraging."""
import numpy as np

class World(object):
    """
    World class, 2D with dimension of x_dim and y_dim.
    There are num_ants in this 2D world.
    This class also contains 1 array for the pheromone level and 1
    array for food.
    """
    def __init__(self, X_dim, Y_dim, Num_ant):
        '''
        Initialize the world class.
        There are three attributes: x_dim, y_dim, and num_ants.
        '''
        self.num_ants = Num_ant
        self.x_dim = X_dim
        self.y_dim = Y_dim
        self.smell = np.zeros((X_dim, Y_dim))
        self.food = np.zeros((X_dim, Y_dim))


    def init_food(self):
        '''
        Define the regions that have food. There are two squares.
        '''
        # ==> Create two blocks of food each 5*5
        self.food[45:50, 25:30] = 10
        self.food[60:65, 5:10] = 10


    def get_w_dim(self):
        '''
        return the dimension of the 2D world. The output contains two values.
        '''
        return self.x_dim, self.y_dim


    def get_num_ants(self):
        '''
        return the number of ants.
        '''
        return self.num_ants
