import numpy as np
import random 

class Ant(object):
    
    def __init__(self, W_dim):
        self.Lx = W_dim[0]
        self.Ly = W_dim[1]
        self.x = np.random.randint(0, self.Lx)
        self.y = np.random.randint(0, self.Ly)
        self.has_food = 0
        return
    
    def go_back_home (self):
        pick = np.zeros(self.x + self.y)
        pick[0:self.x] = 1          
        if ( np.random.choice(pick) == 1 ):
            self.x = self.x - 1
        else:
            self.y = self.y - 1

        # Prevent ants from going out of bound.
        if(self.x < 0):
            self.x = 0
        if(self.y < 0):
            self.y = 0

        return
    
    def get_ant_loc(self):
        return self.x, self.y
        
    def foraging(self, smlx, smly):
        # Check to see if there is pheromone around.
        g = [] 
        m = [] 
            
        # Ant checks the next grid to the right and smells it if there is pheromone. 
        if ( self.x + 1 < self.Lx):
            if ( smlx > 0 ):
                m.append([self.x+1, self.y])
                g.append('right')
                    
        # Ant checks the next grid to the up and smells it if there is pheromone. 
        if ( self.y + 1 < self.Ly):
            if ( smly > 0 ):
                m.append([self.x, self.y+1])
                g.append('up')
                    
        # Ant follows the bigger smell.       
        if ( g != [] ):
            grad = g[m.index(max(m))]
        else:
            grad = random.choice(['up', 'left', 'down', 'right'])
                
        # Move the ant in one of the four directions.
        if ( grad == 'up' ):
            self.y = self.y + 1
        elif ( grad == 'right' ):
            self.x = self.x + 1
        elif ( grad == 'down' ):
            self.y = self.y - 1
        elif ( grad == 'left' ):
            self.x = self.x - 1
        else:
            print(grad)
            print("ERROR!!!!!!!!!!!!")

                
        # Prevent ants from going out of bound.
        if ( self.x < 0 ):
            self.x = 0
        if ( self.y < 0 ):
            self.y = 0
        if ( self.x > self.Lx - 1 ):
            self.x = self.Lx - 1 
        if ( self.y > self.Ly - 1  ):
            self.y = self.Ly - 1 
            
        return