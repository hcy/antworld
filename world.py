
import numpy as np



class World(object):
    
    ''' This is a moving particle class '''
    
    def __init__(self , X_dim, Y_dim, Num_ant):
        self.num_ants = Num_ant
        self.x_dim = X_dim
        self.y_dim = Y_dim
        self.smell = np.zeros((X_dim,Y_dim))
        self.food = np.zeros((X_dim,Y_dim))
        return
        
    def init_food (self):
        
        # ==> Create two blocks of food each 5*5 
        self.food[45:50, 25:30] = 10
        self.food[60:65, 5:10] = 10
        return
    
    
    def get_w_dim(self):
        return self.x_dim, self.y_dim
        
    
    def get_num_ants(self):
        return self.num_ants    
        
        